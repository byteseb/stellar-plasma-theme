# Stellar Plasma Theme


Conceptual Plasma Style based on Breeze, Just tries to improve consistency, contrast and make everything look a little more "modern", but remaining that Breeze essence.
Only works for the Plasma Shell for now, not apps. Needs possible tweaking and fixing.

**Screenshots**

| Light Theme | Dark Theme |
| ------ | ------ |
| ![Light Theme](Screenshots/Light.png) | ![Dark Theme](Screenshots/Dark.png) |

**What this Plasma Style addresses**

1. Too many lines. It seems like this is the path we're taking. Cleaning everything up could hurt contrast, as the outlines gave that contrast, and the current color palette has the Buttons' color very similar to the background. I propose to replace most outlines with different colors and shadows, only leaving outlines for things that are really necessary (focus states, LineEdit, separators).

2. Too flat. Breeze already has shadows for controls, but it seems that many didn't even know, because the outline makes them hard to see. Using shadows, like in this proposal, gives more depth, makes the system look more dynamic and provides better contrast. State changes in buttons are easier to notice, because the shadow gives a raised effect, and when pressed, "it goes down".

3. Inconsistent. For this Plasma Style, I used consistent units as much as possible:

**Units used**

| State | Value |
| ------ | ------ |
| Radius | 5px |
| Radius for Bigger objects/Focus outline | 7px |
| Neutral/Low Importance/ToolButton Hover Color | .ColorScheme-Text, 20% Alpha
| Buttons, LineEdit, Slider/Switch Handles Background | .ColorScheme-ButtonBackground, 100% Alpha
| Hover  | .ColorScheme-ButtonHover, 30% Alpha        |
| Medium Importance (Usually, selected)        | .ColorScheme-ButtonHover, 50% Alpha        |
| High Importance (Also, Selected + Pressed, or just pressed) | .ColorScheme-ButtonHover, 70% Alpha |

I'm human, and spent a lot of hours manually, and repetitively changing everything, so it's possible I committed some inconsistencies, but that is easy to fix :p

** What did I make it this way? **

Taking a look at Plasma users' comments and posts...

"Can we please get rid of the glow flames around literally everything?

EDIT: oh apparently it's gone from the main views but it's still used when you hover over buttons - apparently. It would be much nicer to just lighten the color of the whole button on hover instead of drawing an outline. This is what all the other platforms do. " :

https://www.reddit.com/r/kde/comments/ml5soq/comment/gtjtl7a/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button

"Breeze is hella fine as it is.

The only issue i got with breeze is that it is too angular. Too many lines make it look way too squared. If they removed the lines and hedges, and make it more flat, it would be perfect. " :

https://www.reddit.com/r/kde/comments/ml5soq/comment/gtjyr13/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button

"I don't have a problem with the Breeze visual style, but the icons are very ugly and dated. Unfortunately, no third-party icon pack comes close to the coverage of the Breeze icons, so there's not much choice there. Also, the plasma panel could use a UI redesign. Customizing it is needlessly complex and prone to crashing. "

https://www.reddit.com/r/kde/comments/15rv037/comment/jwbnh8g/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button. 

Some other contributors are already doing a great job on modernizing the icons, making them more legible and rounded. This change would fit.

Also, see "Rounded or Sharp-Corner Buttons?":

https://uxplanet.org/rounded-or-sharp-corner-buttons-def3977ed7c4

"I am overall a happy Plasma user but I also agree it doesn't look as slick as it could. Some comments here are saying other modernized UIs look bad but that's missing the point and muddying the conversation - bringing up examples of badly modernized UIs does not change that Plasma could use some polish to become even better. " :

(https://www.reddit.com/r/kde/comments/15rv037/comment/jwcc6y1/?utm_source=share&utm_medium=web3x&utm_name=web3xcss&utm_term=1&utm_content=share_button)


Please, remember that this is just a proposal, not a final thing. With feedback, this could improve, or even be discarded completely in favor of something that most people would like to have.
It's not "change for the sake of change". It's just trying to follow a more coherent vision. More of an evolution instead of a revolution.

**Notes!**

This is just a Plasma Style, meaning that it only applies for the shell. If this becomes the default after polishing, we would still need somebody to turn it into an Application style, so that it is actually used in apps. This requires C++ code, if I'm not wrong. I don't really dominate that language, so I could not help a lot with that, other than being as transparent as possible on how I made the Plasma Style, contributing to the documentation and Spanish translation.

There were some elements that I didn't modify, such as all of the background files, tooltip, frame, line, configuration icons, toolbar, monitor, containment controls, action button, branding and analog meter. I didn't change the background because the corner radius looked weirder as the radius increased. I would have really liked to change it, but it looks like this is related to a bug regarding anti-aliasing missing from corners, regardless of my theme and corner radius. 
The other mentioned files were not changed because I don't know where they are used, or if we're going to deprecate them. Some look really old and not used anymore.

I broke consistency in some controls. For example, Pressed Checkboxes, RadioButtons and Scrollbars are fully solid, to avoid weird transparency effects and improve contrast. Also, would we want to hardcode checkboxes and radio buttons' checkmarks/dot, and the slider handle to always be white? They probably look more natural that way.

For the Task Switcher, I made the radius more subtle, because I could end up changing the layout and modifying the size. That would be even more controversial.

For tabbed elements (see https://invent.kde.org/plasma/plasma-desktop/-/merge_requests/1946), I added a background because it could get hard to see what you were selecting... But now, that highlights padding inconsistencies between applets that don't rely on the Plasma Style and were always there, just hardly noticeable because tabs were hard to notice themselves. For example:

![Screenshots/Issues](Screenshots/Issues/ThinTab.png)

![Screenshots/Issues/ThinTab.png](Screenshots/Issues/ThinTab2.png)

How it should look:

![CorrectTab](Screenshots/Issues/CorrectTab.png)


For switches, I would have liked to use something more similar to this (without the checkmark):

![Switch](Screenshots/Issues/Switch.png)

But it looks like switches are hardcoded to always have the tinted background visible, it just gets very thin, and the handle is bigger, so it hides it, but prevents making a more balanced/compact switch with inner handle. Also, it seems like the hovered state for the switch is bugged, detecting it even when the mouse is not hovering, and the applet has been closed.

**Installation**

Download all the folders under /Stellar.
Move the desktoptheme/stellar (only stellar folder, to avoid overwriting all your previously downloaded Plasma Styles) folder to ~/.local/share/plasma/desktoptheme.
To have better results, downloading the 2 Color Scheme files is recommended. Move them to ~/.local/share/color-schemes/